import requests
import os, json
import hashlib
import pandas as pd
import sqlite3


def processor():
    url = "https://restcountries-v1.p.rapidapi.com/all"

    headers = {
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
        'x-rapidapi-key': "4f948af52cmshf2f3ed59448f51dp195a5fjsne3a1d208e638"
    }
    response = requests.request("GET", url, headers=headers)
    response_native = json.loads(response.text)

    for region in response_native:
        if region['region'] == 'Africa' and region['name'] == 'Angola':

            alpha3Code = region['alpha3Code']
            url = f'https://restcountries.eu/rest/v2/alpha/{alpha3Code}'
            country_region = requests.\
                request("GET", url, headers=headers)

            country_region_json = json.loads(country_region.text)
            languages_country = country_region_json['languages']

            for language_country in languages_country:
                string_language = str(language_country['name'])
                hash_result = hashlib.sha1(string_language.encode())

            data = {
                'Region': [country_region_json['region']],
                'City_Name': [country_region_json['name']],
                'Languaje': [hash_result.hexdigest()]
            }

            df = pd.DataFrame(data, columns=['Region', 'City_Name', 'Languaje'])

            print('Dataframe..............')
            print(df)

            df.to_json(r'data/data.json')
            path_to_json = r'data/'
            count = 0

            # Connecting to sqlite
            conn = sqlite3.connect('regions.db')

            # Creating a cursor object using the cursor() method
            cursor = conn.cursor()

            # Doping EMPLOYEE table if already exists.
            cursor.execute("DROP TABLE IF EXISTS REGION")

            # Creating table as per requirement
            sql = '''CREATE TABLE REGION(
               ID_REGION INT NOT NULL PRIMARY KEY,
               REGION_NAME CHAR(50) NOT NULL,
               CITY_REGION_NAME CHAR(50) NOT NULL,
               LANGUAGE_CITY_REGION CHAR(50) NOT NULL
            )'''

            cursor.execute(sql)
            print("Table created successfully...........")
            sql_insert = ''' INSERT INTO REGION(ID_REGION,REGION_NAME,CITY_REGION_NAME,LANGUAGE_CITY_REGION)
                          VALUES(?,?,?,?) '''

            # Data insert method
            for file_name in [file for file in os.listdir(path_to_json) if file.endswith('.json')]:
                with open(path_to_json + file_name) as json_file:
                    data = json.load(json_file)
                    data_insert = [str(count),
                                   data['Region'][str(count)],
                                   data['City_Name'][str(count)],
                                   data['Languaje'][str(count)]]
                    count += 1

                cursor.execute(sql_insert, data_insert)

            # Commit your changes in the database
            conn.commit()

            # Select * from REGION
            print("Query REGION..............")
            cursor.execute('''SELECT * FROM REGION;''')
            rows = cursor.fetchall()
            for row in rows:
                print(row)

            # Closing the connection
            conn.close()


if __name__ == '__main__':
    processor()

